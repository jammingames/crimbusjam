﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HSVRainbow : MonoBehaviour
{
    public float speed = 1.0f;
    SpriteRenderer rend;
    float currentHue = 0;
    Color origCol;
    private void Awake()
    {
        rend = GetComponent<SpriteRenderer>();
        origCol = rend.material.color;
        rend.material.color = origCol;
        rend.material.EnableKeyword("_EMISSION");
    }

    private void Update()
    {
        currentHue = Mathf.Repeat(Time.time * speed, 1);
        rend.material.SetColor("_EmissionColor", Color.HSVToRGB(currentHue, 1, 1));
    }
}
