﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TentacleManController : MonoBehaviour
{

    public Transform target;
    public bool shouldMove = false;
    public float minDistance = 2;
    public float maxDistance = 10;
    public float minAlpha = 0.0f;
    public Vector3 movePos;
    Vector3 origPos;
    SpriteRenderer sprRenderer;
    Color origColor;
    Color finalColor;

    bool shouldRun = false;
    private void Awake()
    {
        if (target == null)
        {
            target = Camera.main.transform;
        }
        origPos = transform.position;
        sprRenderer = GetComponent<SpriteRenderer>();
        origColor = sprRenderer.color;
        finalColor = origColor;
        finalColor.a = minAlpha;
        origPos = transform.localPosition;
        shouldRun = true;
    }

    void Update()
    {
        if (!shouldRun)
            return;
        if (Vector3.Distance(target.position, transform.position) < maxDistance)
        {
            float inverseT = Mathf.InverseLerp(maxDistance, minDistance, Vector3.Distance(target.position, transform.position));
            sprRenderer.color = (Color.Lerp(origColor, finalColor, inverseT));
            if (shouldMove)
                transform.localPosition = Vector3.Lerp(origPos, movePos, inverseT);
        }
    }
}
