﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class InventoryItemUI : MonoBehaviour
{
    public string itemName;
    public Image image;
    [HideInInspector]
    public Toggle toggle;


    Item item;
    Color origColor;
    Color usedColor;
    Color clearColor;
    bool itemGiven = false;
    bool toggleItemGiving = false;
    bool isInitialized = false;

    private IEnumerator Start()
    {
        yield return new WaitForEndOfFrame();
        if (!isInitialized)
            Init();
    }

    private void OnEnable()
    {
        ItemManager.OnItemGiveSuccess += HandleItemSuccess;
    }

    private void OnDisable()
    {
        ItemManager.OnItemGiveSuccess -= HandleItemSuccess;
    }

    private void HandleItemSuccess(Item item)
    {
        if (item.name == itemName)
            ItemGiven(itemName);
    }

    void Init()
    {
        toggle = GetComponent<Toggle>();
        item = ItemManager.instance.GetItem(itemName);
        image.sprite = item.image;
        origColor = image.color;
        usedColor = Color.black;
        clearColor = image.color;
        clearColor.a = 0;
        image.color = clearColor;
        image.sprite = item.image;
        if (toggle == null)
            toggle = GetComponent<Toggle>();
        if (toggle == null)
            toggle = GetComponentInChildren<Toggle>();
        if (toggle != null)
            toggle.interactable = false;
        isInitialized = true;
    }

    public void Enable(string itemName)
    {
        if (item.name == itemName)
        {
            image.color = origColor;
            toggle.interactable = true;
        }
    }

    public void GiveItem()
    {
        if (ItemManager.instance.currentCharacter != null)
        {
            ItemManager.instance.GiveItem(itemName, ItemManager.instance.currentCharacter);
        }
    }

    public void ItemGiven(string itemName)
    {
        if (itemName != this.itemName)
        {
            print("incorrect item");
            return;
        }
        image.color = usedColor;
        toggle.interactable = false;
        itemGiven = true;
        ToggleGiveItem(false);
    }

    public bool HasGivenItem()
    {
        return itemGiven;
    }

    public void ToggleGiveItem(bool isActive)
    {
        toggleItemGiving = isActive;
    }
}
