﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ItemManager : Singleton<ItemManager>
{
    public delegate void GameEvent();

    public static event GameEvent OnAllItemsGiven;

    public delegate void ItemEvent(Item item);

    public static event ItemEvent OnItemGiven, OnItemGiveSuccess, OnItemPickedUp;

    public List<Item> items;
    List<InventoryItemUI> itemsUI = new List<InventoryItemUI>();
    List<Item> tempItems;
	List<Item> itemsHeld;
    public GameObject inventoryUIElementPrefab;
    public Character currentCharacter;
    int numItemsGiven;
    int totalItems;
    [HideInInspector]
    public bool hasBackpack = false;

    private void Awake()
    {
        itemsUI = new List<InventoryItemUI>(FindObjectsOfType<InventoryItemUI>());
        //tempItems = new List<Item>();

        tempItems = items;
		itemsHeld = new List<Item>();
        totalItems = items.Count;
    }

    public void AddItem(Item newItem)
    {
		if (!itemsHeld.Contains(newItem))
		{
			itemsHeld.Add(newItem);
			print("added " + newItem.name + " to itemsHeld. Count is: " + itemsHeld.Count);
		}
       foreach(InventoryItemUI itemUI in itemsUI)
        {
            itemUI.Enable(newItem.name);
        }
    }

    public Item GetItem(string name)
    {
        return tempItems.Find(x => x.name.Contains(name));
    }

    public void GiveItem(string itemName, Character character)
    {
        Item item = GetItem(itemName);
        if (item == null)
        {
            print("could not give item : " + itemName + " was not found");
            return;
        }
        //character.ReceiveItem(item);
        OnItemGiven?.Invoke(item);
        
        
    }

    public void TriggerItemGiveSuccess(Item item)
    {
        OnItemGiveSuccess?.Invoke(item);
        tempItems.Remove(item);
        numItemsGiven++;
        print(numItemsGiven + " items given total out of " + totalItems);
        if (numItemsGiven >= totalItems)
        {
            OnAllItemsGiven?.Invoke();
        }
    }

    public void TriggerItemPickup(Item item)
    {
        OnItemPickedUp?.Invoke(item);
    }

    public void TryGiveItem()
    {
        foreach(InventoryItemUI itemUI in itemsUI)
        {
            if (itemUI.toggle.isOn)
            {
                print("trying to give  " + itemUI.name);
                itemUI.GiveItem();
            }
        }
    }

    public void ShowInventory()
    {
        inventoryUIElementPrefab.SetActive(true);
    }

    public void HideInventory()
    {
        inventoryUIElementPrefab.SetActive(false);
    }
}
