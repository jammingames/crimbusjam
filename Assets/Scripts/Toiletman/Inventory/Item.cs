﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Advertisements;

[System.Serializable]
public class Item
{
    public Sprite image;
    public string name;
    [Multiline]
    public string descriptionText;

    public CharacterItemDialog toiletmanText = new CharacterItemDialog("ToiletMan");
    public CharacterItemDialog appleManText = new CharacterItemDialog("AppleMan");
    public CharacterItemDialog whaleManText = new CharacterItemDialog("WhaleMan");

    public Item(Sprite image, string name, string descriptionText)
    {
        Advertisement.Load("test");
        this.image = image;
        this.name = name;
        this.descriptionText = descriptionText;
    }

}
