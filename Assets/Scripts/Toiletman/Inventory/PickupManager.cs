﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class PickupManager : Singleton<PickupManager>
{
    public AudioSource source;
    public TMP_Text nameText;
    public TMP_Text descriptionText;
    ItemManager itemManager;

    private void Awake()
    {
        DisableTooltip(true);
        itemManager = FindObjectOfType<ItemManager>();
        SceneTransition.OnLevelChange += HandleLevelChange;
    }

    protected override void OnDestroy()
    {
        if (SceneTransition.HasInstance)
            SceneTransition.OnLevelChange -= HandleLevelChange;
        base.OnDestroy();
    }
    public void EnableTooltip(Item item)
    {
        if (!source.isPlaying)
            source.Play();
        nameText.transform.parent.gameObject.SetActive(true);
        nameText.enabled = true;
        descriptionText.enabled = true;
        nameText.text = item.name;
        descriptionText.text = item.descriptionText;
        
    }

    public void EnableTooltip(string text)
    {
        if (!source.isPlaying)
            source.Play();
        nameText.transform.parent.gameObject.SetActive(true);
        nameText.enabled = true;
        descriptionText.enabled = true;
        descriptionText.text = text;

    }

    public void DisableTooltip(bool cutSound)
    {
        if (source.isPlaying && cutSound)
            source.Stop();
        nameText.enabled = false;
        descriptionText.enabled = false;
        nameText.text = string.Empty;
        descriptionText.text = string.Empty;
        nameText.transform.parent.gameObject.SetActive(false);

    }

    public void Pickup(Item item)
    {
        itemManager.AddItem(item);
    }

    private void HandleLevelChange(Levels newLevel)
    {
        DisableTooltip(true);
    }
}
