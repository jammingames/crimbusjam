﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PickupItem : MonoBehaviour, ITriggerable, IRaycastInteractable
{
    public Item item;
    public string name;
    public bool pickupable = true;
    bool canPickup = false;
    bool isFocused = false;
    PickupManager pickupManager;
    bool lookAt = true;
    Transform target;
    HSVRainbow highlight;
    void Awake()
    {
        pickupManager = FindObjectOfType<PickupManager>();
        name = item.name;
        highlight = GetComponentInChildren<HSVRainbow>();
    }

    void Update()
    {
        if (highlight != null && pickupable)
        {
            if (!highlight.gameObject.activeInHierarchy && isFocused)
            { 
                highlight.gameObject.SetActive(true);
            }
            else if (highlight.gameObject.activeInHierarchy && !canPickup && !isFocused)
            {
                highlight.gameObject.SetActive(false);
            }
        }
        if (Input.GetKeyDown(KeyCode.E) && canPickup && isFocused && pickupable)
        {
            pickupManager.Pickup(item);
            canPickup = false;
            ItemManager.instance.TriggerItemPickup(item);
            Destroy(gameObject);
            pickupManager.DisableTooltip(false);
        }
    }
    public void OnEnter()
    {
        //print("entered " + gameObject.name);
        canPickup = true;
    }

    public void OnExit()
    {
        //print("exit item");
        //print("exit " + gameObject.name);
        canPickup = false;
    }

    public void OnGazeEnter()
    {
        isFocused = true;
        pickupManager.EnableTooltip(item);
    }

    public void OnGazeExit()
    {
        isFocused = false;
        pickupManager.DisableTooltip(true);
    }

    void OnDisable()
    {
        canPickup = false;
        isFocused = false;
    }

}