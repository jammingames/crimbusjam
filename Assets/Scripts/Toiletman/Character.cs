﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class Character : MonoBehaviour, ITriggerable, IRaycastInteractable
{
    public string name;
    bool inRange = false;
    bool lookingAt = false;
    [HideInInspector]
    public bool firstInteraction = true;
    [Multiline]
    public string dialog;

    void OnEnable()
    {
        ItemManager.OnItemGiven += HandleItemGiven;
    }

    void OnDisable()
    {
        ItemManager.OnItemGiven -= HandleItemGiven;
    }

    void Update()
    {
        if (Input.GetKeyDown(KeyCode.E) && lookingAt && inRange)
        {
            print("TRYING TO GIVE ITEM TO : " + name);
            ItemManager.instance.TryGiveItem();
        }
    }

    private void HandleItemGiven(Item item)
    {
        ReceiveItem(item);
    }

    public void ReceiveItem(Item item)
    {   
        //DialogueManager.instance.EnablePopup
        //item.appleManText;
        //do dialogue for said item
        CharacterItemDialog newDialog = new CharacterItemDialog("default");
        switch(name)
        {
            //characterdial
            case "appleman":
                newDialog = item.appleManText;
                break;
            case "whaleman":
                newDialog = item.whaleManText;
                break;
            case "toiletman":
                newDialog = item.toiletmanText;
                break;
        }

        DialogueManager.instance.EnablePopup(newDialog.dialog);
        if (newDialog.takesItem)
        {
            ItemManager.instance.TriggerItemGiveSuccess(item);
            firstInteraction = false;
        }
    }

    public void OnEnter()
    {
        inRange = true;
        if (lookingAt && firstInteraction)
        {
            ItemManager.instance.currentCharacter = this;
            DialogueManager.instance.EnablePopup(dialog);
        }
    }

    public void OnExit()
    {
        inRange = false;
        if (ItemManager.instance.currentCharacter == this)
            ItemManager.instance.currentCharacter = null;

        PickupManager.instance.DisableTooltip(false);

    }

    public void OnGazeEnter()
    {
        lookingAt = true;
        DialogueManager.instance.EnablePopup(dialog);
    }

    public void OnGazeExit()
    {
        lookingAt = false;
        PickupManager.instance.DisableTooltip(false);        
        ItemManager.instance.currentCharacter = null;
        DialogueManager.instance.DisablePopup();
        firstInteraction = false;
        
    }
  
}
