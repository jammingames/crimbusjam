﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CharacterTrigger : MonoBehaviour
{
    private void OnTriggerEnter(Collider other)
    {
        if (other.GetComponent<ITriggerable>() != null)
        {
            other.GetComponent<ITriggerable>().OnEnter();
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.GetComponent<ITriggerable>() != null)
        {
            other.GetComponent<ITriggerable>().OnExit();
        }
    }


}
