﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface IRaycastInteractable
{
    void OnGazeEnter();
    void OnGazeExit();

}
