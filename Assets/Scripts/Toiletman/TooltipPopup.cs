﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class TooltipPopup : MonoBehaviour, IRaycastInteractable
{
    public bool cutSound = false;
    [Multiline]
    public string tooltipText;

    public void OnGazeEnter()
    {
        print("enter gaze");
        TooltipManager.instance.EnableTooltip(tooltipText);
    }

    public void OnGazeExit()
    {
        TooltipManager.instance.DisableTooltip(cutSound);
        print("gaze exited");
    }

}
