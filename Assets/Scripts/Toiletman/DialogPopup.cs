﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class DialogLine
{
    public string itemName;
    [Multiline]
    public string textLine;
    public bool isConsumed = false;
}

public class DialogPopup : MonoBehaviour, ITriggerable
{
    [Multiline]
    public List<DialogLine> dialogLines;
    DialogueManager dialogueManager;

    void Awake()
    {
        dialogueManager = FindObjectOfType<DialogueManager>();
    }

    public void OnEnter()
    {
        dialogueManager.EnablePopup(dialogLines);
    }

    public void OnExit()
    {
        dialogueManager.DisablePopup();
    }

}
