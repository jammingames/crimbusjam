﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AssignRandomAnimationClip : MonoBehaviour
{
    Animator thisAnimator;

    private void Awake()
    {
        thisAnimator = GetComponent<Animator>();
        thisAnimator.Play("idle", 0, Random.Range(0, thisAnimator.GetCurrentAnimatorStateInfo(0).length));    
    }
}
