﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CompanionFollow : MonoBehaviour, ITriggerable
{
    public float minDistance = 5;
    public float maxDistance;
    public float speed = 5;
    public Transform target;
    bool shouldFollow = false;
    bool canFollow = false;

    public void OnEnter()
    {
        canFollow = true;
    }

    public void OnExit()
    {
       
    }

   
    void Awake()
    {
        canFollow = false;
        shouldFollow = false;
        if (target == null)
        {
            target = Camera.main.transform;
        }
    }

    private void Update()
    {
        if (!canFollow)
            return;

        if (Vector3.Distance(transform.position, target.position) < minDistance)
        {
            shouldFollow = false;
        }
        else
        {
            Vector3 targetDirection = target.position - transform.position;
            Vector3 newDirection = Vector3.RotateTowards(transform.forward, targetDirection, speed*2*Time.deltaTime, 0.0f);

            shouldFollow = true;
            transform.position = Vector3.Lerp(transform.position, target.position, Time.deltaTime * speed);
            transform.rotation = Quaternion.LookRotation(newDirection);
        }
    }
}
