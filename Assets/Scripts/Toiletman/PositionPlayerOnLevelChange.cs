﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using StandardAssets.Characters.FirstPerson;
using StandardAssets.Characters.Physics;
public class PositionPlayerOnLevelChange : MonoBehaviour
{
    public Levels thisLevel;
    SC_FPSController player;
    private void Awake()
    {
        player = FindObjectOfType<SC_FPSController>();
        
        SceneTransition.OnLevelChange += HandlePositionChange;
    }

    private void OnDestroy()
    {
        if (SceneTransition.HasInstance)
            SceneTransition.OnLevelChange -= HandlePositionChange;
    }

    void HandlePositionChange(Levels newLevel)
    {
        if (newLevel == thisLevel)
        {
            player.transform.position = transform.position;
            player.transform.rotation = transform.rotation;
        }
    }

}
