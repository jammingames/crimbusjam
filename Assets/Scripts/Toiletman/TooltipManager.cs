﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using System;

public class TooltipManager : Singleton<TooltipManager>
{
    public AudioSource source;
    public TMP_Text textObject;

    private void Awake()
    {
        DisableTooltip(true);
        SceneTransition.OnLevelChange += HandleLevelChange;
    }

  

    private void OnDestroy()
    {
        if (SceneTransition.HasInstance)
        {
            SceneTransition.OnLevelChange -= HandleLevelChange;
        }
    }
    public void EnableTooltip(string newText)
    {
        if (!source.isPlaying)
            source.Play();
        textObject.enabled = true;
        textObject.text = newText;
    }

    public void DisableTooltip(bool cutSound)
    {
        if (source.isPlaying && cutSound)
            source.Stop();
        textObject.enabled = false;
        textObject.text = string.Empty;
    }

    private void HandleLevelChange(Levels newLevel)
    {
        DisableTooltip(true);
    }
}
