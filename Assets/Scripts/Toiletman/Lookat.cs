﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Lookat : MonoBehaviour
{
    public Transform target;
    public float speed = 5;
    public bool constrainX = false;
    public bool constrainY = false;
    public bool constrainZ = false;

    private void Awake()
    {
        if (target == null)
        {
            target = Camera.main.transform;
        }    
    }
   
    // Update is called once per frame
    void Update()
    {
        Vector3 targetDirection = target.position - transform.position;
        Vector3 newDirection = Vector3.RotateTowards(transform.forward, -targetDirection, speed * Time.deltaTime, 0.0f);
        transform.rotation = Quaternion.LookRotation(newDirection);



        //transform.LookAt(target);
    }
}
