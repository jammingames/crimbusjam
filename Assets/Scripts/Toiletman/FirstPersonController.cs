﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FirstPersonController : MonoBehaviour
{
    bool isJumpKey = true;
    float forwardAxis = 0;
    float sideAxis = 0;
    float mouseXAxis = 0;
    float mouseYAxis = 0;

    void HandleInput()
    {
        if (Input.GetButtonDown("Space"))
        {
            isJumpKey = true;
        }
        if(Input.GetButtonUp("Space"))
        {
            isJumpKey = false;
        }

        if (Input.GetAxis("Horizontal") != 0)
        {
            sideAxis = Input.GetAxis("Horizontal");
        }
        else sideAxis = 0;

        if (Input.GetAxis("Vertical") != 0)
        {
            forwardAxis = 0;
        }
        else forwardAxis = 0;

    }


    private void Update()
    {
        HandleInput();
    }

    private void FixedUpdate()
    {
        
    }
}
