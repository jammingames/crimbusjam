﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using System;

public class DialogueManager : Singleton<DialogueManager>
{
    public AudioSource source;
    public GameObject rootObject;
    public TMP_Text textObject;
    private int currentIndex;
    List<DialogLine> textLines;

    private void Awake()
    {
        DisablePopup();
        SceneTransition.OnLevelChange += HandleLevelChange;
    }

    private void OnDestroy()
    {
        if (SceneTransition.HasInstance)
            SceneTransition.OnLevelChange -= HandleLevelChange;
    }


    private void Update()
    {
        if (Input.anyKeyDown && textLines != null)
        {
            NextPopup();
        }
    }

    public void EnablePopup(List<DialogLine> newText)
    {
        if (newText.Count == 0 || newText == null)
            return;
        textLines = newText;
        if (!source.isPlaying)
            source.Play();
        rootObject.SetActive(true);

        textObject.text = textLines[0].textLine;
        currentIndex = 0;
    }


    public void EnablePopup(string newText)
    {
        
        if (!source.isPlaying)
            source.Play();
        rootObject.SetActive(true);

        textObject.text = newText;
        currentIndex = 0;
    }


    public void NextPopup()
    {
        currentIndex++;
        if (textLines != null && currentIndex < (textLines.Count - 1))
        {
            source.Play();
            textObject.text = textLines[currentIndex].textLine;
        }
        else
        {
            DisablePopup();
        }
    }

    public void DisablePopup()
    {
        textLines = null;
        rootObject.SetActive(false);
        textObject.text = string.Empty;
    }

    private void HandleLevelChange(Levels newLevel)
    {
        DisablePopup();
    }

}
