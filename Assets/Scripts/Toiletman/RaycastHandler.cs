﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RaycastHandler : MonoBehaviour
{
    public LayerMask mask;
    public float distance = 40;


    Transform target;
    IRaycastInteractable currentTarget;
    IRaycastInteractable lastTarget;
    private void FixedUpdate()
    {
        
        if (Physics.Raycast(transform.position, transform.forward, out RaycastHit hit, distance, mask))
        {
            target = hit.transform;
            
            
            if (target.GetComponent<IRaycastInteractable>() != null)
            {
                currentTarget = target.GetComponent<IRaycastInteractable>();
                if (currentTarget != lastTarget)
                {
                    currentTarget.OnGazeEnter();
                    lastTarget = currentTarget;
                }

            }
            else //sees non interactable
            {
                if (currentTarget != null)
                {
                    currentTarget.OnGazeExit();
                }
                currentTarget = null;
            }
        }
        else
        {
            if (currentTarget != null)
            {
                currentTarget.OnGazeExit();
            }
            currentTarget = null;
            lastTarget = null;
        }
    }
}
