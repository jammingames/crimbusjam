﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class CharacterItemDialog
{
    public string characterToGive;
    public bool takesItem = false;
    [Multiline]
    public string dialog;

    public CharacterItemDialog(string newName)
    {
        characterToGive = newName;
     }
}
