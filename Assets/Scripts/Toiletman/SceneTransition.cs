﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public enum Levels
{
    Beginning = 0,
    House = 1,
    Garage = 2,
    Basement = 3,
    Outside = 4,
    WhaleMan = 5,  
}
public class SceneTransition : Singleton<SceneTransition>
{
    public delegate void LevelChange(Levels newLevel);

    public static event LevelChange OnLevelChange;
    
    public Levels currentLevel;
    public Levels startingLevel = Levels.House;

    public Image fadeImage;
    public GameObject[] levels;
    public float duration = 0.5f;
    public EaseType easer;
    Color origCol;
    Color targetCol;
    
    [RuntimeInitializeOnLoadMethod(RuntimeInitializeLoadType.BeforeSceneLoad)]
    private void Awake()
    {
        origCol = fadeImage.color;
        origCol.a = 0;
        targetCol = origCol;
        targetCol.a = 1;
        TriggerLevelEvent(startingLevel);
        StartCoroutine(fadeImage.LerpColor(origCol, duration, easer, null));
    }

    public void ChangeLevel(Levels newLevel)
    {
        if (currentLevel == newLevel)
        {
            print("already in this level");
            return;
        }
        fadeImage.color = origCol;
        fadeImage.gameObject.SetActive(true);
        StartCoroutine(fadeImage.LerpColor(targetCol, duration, easer, () =>
        {
            TriggerLevelEvent(newLevel);
            StartCoroutine(fadeImage.LerpColor(origCol, duration, easer, () =>
            {
                fadeImage.color = targetCol;
                fadeImage.gameObject.SetActive(false);
            }));
        }));
    }

    void TriggerLevelEvent(Levels newLevel)
    {
        for (int i = 0; i < levels.Length; i++)
        {
            levels[i].SetActive((int)newLevel == i);
        }
        print("CHANGING TO " + newLevel);
        OnLevelChange?.Invoke(newLevel);
        currentLevel = newLevel;
    }

    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.Alpha1))
        {
            ChangeLevel(Levels.House);
        }
        if (Input.GetKeyDown(KeyCode.Alpha2))
        {
            ChangeLevel(Levels.Outside);
        }
        if (Input.GetKeyDown(KeyCode.Alpha3))
        {
            ChangeLevel(Levels.Basement);
        }
    }

}
