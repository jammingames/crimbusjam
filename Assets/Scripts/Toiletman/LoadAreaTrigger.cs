﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LoadAreaTrigger : MonoBehaviour, ITriggerable
{
    public Levels levelToLoad;
    bool shouldLoad = true;
    bool inRange = false;
    float delay = 4;

    void OnEnable()
    {
        shouldLoad = false;
        HandleLevelChange(Levels.Beginning);
        SceneTransition.OnLevelChange += HandleLevelChange;
    }


    void OnDisable()
    {
        if (SceneTransition.HasInstance)
            SceneTransition.OnLevelChange -= HandleLevelChange;
        StopAllCoroutines();
    }

    void Update()
    {
        if (Input.GetKeyDown(KeyCode.E) && inRange && shouldLoad)
        {
            SceneTransition.instance.ChangeLevel(levelToLoad);
        }
    }

    public void OnEnter()
    {
        inRange = true;
    }

    public void OnExit()
    {
        inRange = false; 
    }

    void HandleLevelChange(Levels newLevel)
    {
        shouldLoad = false;
        if (gameObject.activeInHierarchy)
            StartCoroutine(Auto.Wait(delay, () => { shouldLoad = true; }));
    }
}
