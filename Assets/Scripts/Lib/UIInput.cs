﻿using UnityEngine;
using System.Collections.Generic;
using UnityEngine.UI;
using System;
using UnityEngine.EventSystems;

public class UIInput : MonoBehaviour, IPointerDownHandler
{
	public List<Action<PointerEventData>> OnMouseDownListeners = new List<Action<PointerEventData>>();

	public void OnPointerDown(PointerEventData eventData)
	{
		foreach (var callback in OnMouseDownListeners)
		{
			callback(eventData);
		}
	}

	public void AddOnMouseDownListener(Action<PointerEventData> action)
	{
		OnMouseDownListeners.Add(action);
	}
}