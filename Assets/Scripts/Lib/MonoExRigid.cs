﻿using System.Collections;
using UnityEngine;


public class MonoExRigid : MonoEx
{
	public float force = 1;
	public float thresholdDistance = 1;

	public override void Reset ()
	{
		StartCoroutine (ResetSequence ());
	}

	IEnumerator ResetSequence ()
	{
		rb.velocity = Vector3.zero;
		rb.useGravity = false;
		while (Vector3.Distance (transform.position, GetOrigPos ()) > thresholdDistance) {
			rb.AddForce ((GetOrigPos () - transform.position) * force, ForceMode.Acceleration);
			yield return null;
		}
		base.Reset ();
		rb.useGravity = true;
		yield return null;
	}
}