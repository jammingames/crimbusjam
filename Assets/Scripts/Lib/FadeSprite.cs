﻿using UnityEngine;
using System.Collections;

public class FadeSprite : MonoBehaviour
{

	SpriteRenderer spr;
	public float duration;
	public bool startInvis = true;
	public bool fadeOutAfter = true;

	public float delay = 9.0f;


	void Awake()
	{
		spr = GetComponent<SpriteRenderer>();
		if (startInvis)
		{
			Color col = spr.color;
			col.a = 0;
			spr.color = col;
		}
	}

	void OnEnable()
	{
		//add your event hook
//		TaskManager.OnNearTask += HandleOnTaskArriving;
	}

	void OnDisable()
	{
		//remove your event hook
//		TaskManager.OnNearTask -= HandleOnTaskArriving;
	}



	//	void HandleOnTaskArriving (Task task, bool isStart)
	//	{
	//		if (task != taskID) return;
	//
	//		if (isStart)
	//			FadeIn();
	//		else
	//			FadeOut();
	//	}
	//
	public void FadeIn()
	{
//		print("fading " + taskID + "  in");
		StartCoroutine(spr.FadeAlpha(0.9f, duration, EaseType.Linear, () => { 
					if (fadeOutAfter)
						FadeOut(0.8f, delay);
				}));
	}

	public void FadeOut()
	{
//		print ("FADING OUT!!!!");
		StartCoroutine(spr.FadeAlpha(0f, duration, EaseType.Linear, null));
	}

	public void FadeOut(float t, float delay)
	{
        //		print ("FADING OUT!!!!");
        StartCoroutine(Auto.Wait(delay, () =>
        {

            StartCoroutine(spr.FadeAlpha(0, t, EaseType.Linear, null));
        }));
	}
}
