﻿using UnityEngine;
using System.Collections;

public class DestroyOnSceneLoad : MonoBehaviour
{

	public int levelToDestroyOn;
	public float delay = 0;

	void OnLevelWasLoaded (int level)
	{
		if (level == levelToDestroyOn) {
			
			GameObject.Destroy (gameObject, delay);

		}
	}
}
