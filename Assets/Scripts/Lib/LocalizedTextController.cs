﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class LocalizedTextController : MonoBehaviour {


	void Start()
	{
		SetAllText();
	}



	void SetAllText()
	{
		LocalizedText[] textFields = GetComponentsInChildren<LocalizedText> (true);

		foreach (LocalizedText text in textFields)
		{
			//			if (text.LocalString == null)				// <<<---- WHY DOES THIS LINE EXIST?????
			text.LocalString = Localizer.instance.GetLocalString(text.name);
		}
	}

}