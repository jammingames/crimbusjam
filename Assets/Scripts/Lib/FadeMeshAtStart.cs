﻿using UnityEngine;
using System.Collections;

public class FadeMeshAtStart : MonoBehaviour
{

	public float target = 0;
	public float start = 1;
	public float duration = 1;
	public float delay = 0.1f;

	//	public EaseType easer;

	Material r;



	void Awake ()
	{
		r = GetComponent<Renderer> ().material;
	}

	void Start ()
	{
		Color c = r.color;
		c.a = start;
		r.color = c;
		StartCoroutine (Auto.Wait (delay, () => {
			StartCoroutine (r.FadeAlpha (target, duration, null));
		}));

	}



		



}
