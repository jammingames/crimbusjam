﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Audio;
using UnityEngine.UI;

public enum ChargeState
{
	idle,
	chargingUp,
	fail,
	succeed,
	superFail,
	recovery
};
/*
 * get reference image's recttransform
 * 
 * 
 */

public class ChargeUp : MonoBehaviour
{
	string chargePitch = "chargePitch";
	[Header("Public References")]
	public Rigidbody appleMan;
	public AnimationCurve animCurveUp;
	public AnimationCurve animCurveDown;
	public AudioSource audioSource;
	public AudioMixer mixer;
	public Slider uiSlider;
	public Slider uiSliderNormalized;
	public Image boxImage;
	[Header("Adjustable Variables")]
	public float maxSpeed = 10;
	public float minSpeed = 1;
	public KeyCode inputKey;
	public float duration = 1;
	public float decelDuration = 0.3f;
	public float timeAugmentFactor = 1;
	public float pitchLowEnd = 0.2f;
	public float pitchHighEnd = 3;
	public Color validColor, badColor;
	[Range(0, 1)]
	public float minValid;
	[Range(0, 1)]
	public float maxValid;

	//private variables
	ChargeState currentState;

	public event ChargeStateChange OnStateChange;
	public delegate void ChargeStateChange();

	float currentStep = 0;
	float normalizedStep = 0; //will be diff for chargeup/chargeDown states

	bool isDown = false; 

	public void SetRangeClamps()
	{
		minValid = Mathf.Clamp(minValid, 0, maxValid);
		maxValid = Mathf.Clamp(maxValid, minValid, 1);
	}

	private void Start()
	{
		SetRangeClamps();
		CalculateAudioPitch();
	}

	private void Update()
	{
		currentStep = Mathf.Clamp(currentStep, 0, duration);
		if (Input.GetKeyDown(inputKey))
		{
			isDown = true;
		}
		if (Input.GetKeyUp(inputKey)) //added getkey check incase game pause and up called while paused
		{
			isDown = false;
			bool success = CheckValidity();
			if (success)
			{
				SetState(ChargeState.succeed);
			}
			else
			{
				SetState(ChargeState.superFail);
			}
			Launch();
		}
		if (!Input.GetKey(inputKey))
		{
			isDown = false;
		}

		if (isDown)
		{
			if (currentStep < duration)
			{
				currentStep += Time.deltaTime;
			}
			normalizedStep = animCurveUp.Evaluate(Mathf.InverseLerp(0, duration, currentStep));
			CalculateAudioPitch();
		}
		else
		{
			if (currentStep > 0)
			{
				currentStep = Mathf.Clamp(currentStep, 0, decelDuration);
				currentStep -= Time.deltaTime;
				//currentStep = Mathf.Clamp(currentStep, 0, duration); /// i know its redundant to clamp it twice
				normalizedStep = animCurveDown.Evaluate(Mathf.InverseLerp(0, decelDuration, currentStep));
				CalculateAudioPitch();
			}
		}
		boxImage.color = CheckValidity() ? validColor : badColor;

		//currentStep = Mathf.Clamp(currentStep, 0, duration); /// i know its redundant to clamp it twice
		SetUISliders();

		if (currentStep <= 0)
		{
			audioSource.volume = 0;
		}
		else
		{
			audioSource.volume = 1;
		}
	}

	void Launch()
	{
		float force = Mathf.Lerp(minSpeed, maxSpeed, normalizedStep);
		appleMan.AddForce(0, force, 0, ForceMode.Impulse);
	}

	void CalculateAudioPitch()
	{
		float desiredPitch = pitchLowEnd + ((pitchHighEnd - pitchLowEnd) * normalizedStep);
		//print("DESIRED AUDIO: " + desiredPitch + "   normalized step: " + normalizedStep + " current step: " + currentStep);
		mixer.SetFloat(chargePitch, desiredPitch);
	}

	void SetUISliders()
	{ 
		if (uiSlider != null)
			uiSlider.value = Mathf.InverseLerp(0, duration, currentStep);
		if (uiSliderNormalized != null)
			uiSliderNormalized.value = normalizedStep;
	}

	void SetBoxColor()
	{
		if (boxImage != null)
			boxImage.color = CheckValidity() ? validColor : badColor;

	}

	bool CheckValidity()
	{
		//if between minvalid and maxvalid
		if (currentStep / duration >= minValid && currentStep / duration <= maxValid)
		{
			print("VALID!!!! " + currentStep/duration);
			return true;
		}
		else
		{
			print("NOPE! " + currentStep/duration);
			return false;
		}
	}

	public void SetState(ChargeState newState)
	{
		if (newState != currentState)
		{
			currentState = newState;
			OnStateChange?.Invoke();
		}
	}
}
