﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.Audio;
using System;
using UnityEngine;

public static class AudioExtensions
{

	/// <summary>
	/// pass in a string, hope to god you don't mistype it (super foolproof)
	/// animate that string's float value with math to make it look purdy
	/// </summary>
	/// <returns>Coroutine to ease float values in audio mixer because fuck inspectors</returns>
	/// <param name="audio">AudioMixer.</param>
	/// <param name="param">Dumb String That Is the Bane of my existence.</param>
	/// <param name="target">Target Float.</param>
	/// <param name="duration">Duration float.</param>
	/// <param name="ease">Ease equation</param>
	public static IEnumerator ChangeAudioParam(
	this AudioMixer audio,
	string param,
	float start,
	float target,
	float duration,
	EaseType ease,
	Action onComplete)
	{

		float elapsed = 0;
		float temp = start;
		float range = target - temp;
		while (elapsed < duration)
		{
			elapsed = Mathf.MoveTowards(elapsed, duration, Time.deltaTime);
			temp = (start + range * Ease.FromType(ease)(elapsed / duration));
			audio.SetFloat(param, temp);
			yield return 0;
		}
		audio.SetFloat(param, target);
		if (onComplete != null)
			onComplete();

	}




	public static IEnumerator ChangePitch(
			this AudioSource audio,
			float start,
			float target,
			float duration,
	EaseType ease,
	Action onComplete)
	{

		float elapsed = 0;
		float temp = start;
		float range = target - temp;
		while (elapsed < duration)
		{
			elapsed = Mathf.MoveTowards(elapsed, duration, Time.deltaTime);
			temp = (start + range * Ease.FromType(ease)(elapsed / duration));
			audio.pitch = temp;
			yield return 0;
		}
		audio.pitch = target;
		if (onComplete != null)
			onComplete();

	}

	public static IEnumerator ChangeVolume(
			this AudioSource audio,
			float start,
			float target,
			float duration,
			System.Action onComplete)
	{
		float elapsed = 0;
		float temp = start;
		float range = target - temp;
		while (elapsed < duration)
		{
			elapsed = Mathf.MoveTowards(elapsed, duration, Time.deltaTime);
			temp = start + range * (elapsed / duration);
			audio.volume = temp;
			yield return 0;
		}

		if (onComplete != null)
			onComplete();

	}
}
